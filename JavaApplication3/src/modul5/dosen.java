/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul5;

/**
 *
 * @author admin
 */
public class dosen {
    private String npp;
    private String nama;
    private String alamat;
    private String email;

    public dosen(String npp, String nama, String alamat, String email) {
        this.npp = npp;
        this.nama = nama;
        this.alamat = alamat;
        this.email = email;
    }

    public String getNpp() {
        return npp;
    }

    public void setNpp(String npp) {
        this.npp = npp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
