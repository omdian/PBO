/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul5;

/**
 *
 * @author admin
 */
public class programstudi {
    private String kode_prodi;
    private String nama_prodi;
    private String ketua_prodi;

    public programstudi(String kd, String nmp) {
        kode_prodi=kd;
        nama_prodi=nmp;
    }

    public void setKode_prodi(String kode_prodi) {
        this.kode_prodi = kode_prodi;
    }

    public void setNama_prodi(String nama_prodi) {
        this.nama_prodi = nama_prodi;
    }

    public void setKetua_prodi(String ketua_prodi) {
        this.ketua_prodi = ketua_prodi;
    }

    public String getKode_prodi() {
        return kode_prodi;
    }

    public String getNama_prodi() {
        return nama_prodi;
    }

    public String getKetua_prodi() {
        return ketua_prodi;
    }
    
    
}
