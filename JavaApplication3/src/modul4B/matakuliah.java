/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul4B;

/**
 *
 * @author admin
 */
public class matakuliah {
    private String kode;
    private String namaMtk;
    private String sks;
    private String namaDosen;

    public matakuliah(String kode, String namaMtk, String sks, String namaDosen) {
        this.kode = kode;
        this.namaMtk = namaMtk;
        this.sks = sks;
        this.namaDosen = namaDosen;
    }

    public String getKode() {
        return kode;
    }

    public String getNamaMtk() {
        return namaMtk;
    }

    public String getSks() {
        return sks;
    }

    public String getNamaDosen() {
        return namaDosen;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public void setNamaMtk(String namaMtk) {
        this.namaMtk = namaMtk;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public void setNamaDosen(String namaDosen) {
        this.namaDosen = namaDosen;
    }
    
    
}
