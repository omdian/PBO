/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul4B;

/**
 *
 * @author admin
 */
public class mobil {
    private String noPolisi;
    private String merk;
    private String warna;
    private double volumeMesin;
    private double kecepatanAwal;
    private double percepatan;

    public mobil(String noPolisi, String merk, String warna, double volumeMesin, double kecepatanAwal, double percepatan) {
        this.noPolisi = noPolisi;
        this.merk = merk;
        this.warna = warna;
        this.volumeMesin = volumeMesin;
        this.kecepatanAwal = kecepatanAwal;
        this.percepatan = percepatan;
    }

    public String getNoPolisi() {
        return noPolisi;
    }

    public String getMerk() {
        return merk;
    }

    public String getWarna() {
        return warna;
    }

    public double getVolumeMesin() {
        return volumeMesin;
    }

    public double getKecepatanAwal() {
        return kecepatanAwal;
    }

    public double getPercepatan() {
        return percepatan;
    }

    public void setNoPolisi(String noPolisi) {
        this.noPolisi = noPolisi;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public void setVolumeMesin(double volumeMesin) {
        this.volumeMesin = volumeMesin;
    }

    public void setKecepatanAwal(double kecepatanAwal) {
        this.kecepatanAwal = kecepatanAwal;
    }

    public void setPercepatan(double percepatan) {
        this.percepatan = percepatan;
    }
    
    public double jarakyangditempuh(int t){
        return (kecepatanAwal*t)+(0.5*(percepatan*Math.pow(t, 2)));
    }
    
}
