/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul4B;

/**
 *
 * @author admin
 */
public class mahasiswa {
    private String nim;
    private String namaMahasiswa;
    private String email;
    private float nilai1;
    private float nilai2;
    private float nilai3;

    public mahasiswa(String nim, String namaMahasiswa, String email, float nilai1, float nilai2, float nilai3) {
        this.nim = nim;
        this.namaMahasiswa = namaMahasiswa;
        this.email = email;
        this.nilai1 = nilai1;
        this.nilai2 = nilai2;
        this.nilai3 = nilai3;
    }

    public String getNim() {
        return nim;
    }

    public String getNamaMahasiswa() {
        return namaMahasiswa;
    }

    public String getEmail() {
        return email;
    }

    public float getNilai1() {
        return nilai1;
    }

    public float getNilai2() {
        return nilai2;
    }

    public float getNilai3() {
        return nilai3;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public void setNamaMahasiswa(String namaMahasiswa) {
        this.namaMahasiswa = namaMahasiswa;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNilai1(float nilai1) {
        this.nilai1 = nilai1;
    }

    public void setNilai2(float nilai2) {
        this.nilai2 = nilai2;
    }

    public void setNilai3(float nilai3) {
        this.nilai3 = nilai3;
    }
    
    public float hitungNil(){
        return(nilai1 + nilai2 + nilai3)/ 3;
    }
}
