/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul4A;

/**
 *
 * @author admin
 */
public class mainstudent {
    public static void main(String[] args) {
        Student mhs1 = new Student("Jono",76,85,90);
        Student mhs2 = new Student("Antok",77,86,88);
        Student mhs3 = new Student("Bambang",90,100,8);
        Student mhs4 = new Student("Agung",90,90,80);
        Student mhs5 = new Student("Albert",90,90,80);
        
        System.out.println("Nama dan rata-rata nilai mahasiswa");
        System.out.println(mhs1.getNama()+" = "+mhs1.getRatarata());
        System.out.println(mhs2.getNama()+" = "+mhs2.getRatarata());
        System.out.println(mhs3.getNama()+" = "+mhs3.getRatarata());
        System.out.println(mhs4.getNama()+" = "+mhs4.getRatarata());
        System.out.println(mhs5.getNama()+" = "+mhs5.getRatarata());
    }
}
