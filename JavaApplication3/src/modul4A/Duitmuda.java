/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul4A;

/**
 *
 * @author admin
 */
public class Duitmuda {
    private int nip;
    private String nama;
    private int gajipokok;
    private int jamlembur;
    private int jumlahanak;
    private int gajitotal;

    public Duitmuda() {
    }


    public void setNip(int nip) {
        this.nip = nip;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setGajipokok(int gajipokok) {
        this.gajipokok = gajipokok;
    }

    public void setJamlembur(int jamlembur) {
        this.jamlembur = jamlembur;
    }

    public void setJumlahanak(int jumlahanak) {
        this.jumlahanak = jumlahanak;
    }

    public void setGajitotal(int gajitotal) {
        this.gajitotal = gajitotal;
    }

    public int getNip() {
        return nip;
    }

    public String getNama() {
        return nama;
    }

    public int getGajipokok() {
        return gajipokok=5000000;
    }

    public int getJamlembur() {
        return jamlembur*5000;
    }

    public int getJumlahanak() {
        return jumlahanak*500000;
    }

    public int getGajitotal() {
        gajitotal = getGajipokok() + getJumlahanak() + getJamlembur();
        return gajitotal;
    }
    
    
    
}
