/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul4A;

/**
 *
 * @author admin
 */
public class Student {
    private String nama;
    private double ujian1,ujian2,ujian3;

    public Student(String nama, double ujian1, double ujian2, double ujian3) {
        this.nama = nama;
        this.ujian1 = ujian1;
        this.ujian2 = ujian2;
        this.ujian3 = ujian3;
    }



    public String getNama() {
        return nama;
    }

    public double getUjian1() {
        return ujian1;
    }

    public double getUjian2() {
        return ujian2;
    }

    public double getUjian3() {
        return ujian3;
    }
    
    public double getRatarata(){
        return (ujian1 + ujian2 + ujian3)/ 3;
    }
    
    
}
