/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul31218;

/**
 *
 * @author admin
 */
public class HMJ {
    private mahasiswa ketua;
    private mahasiswa sekre;
    private mahasiswa bendahara;
//    private String ShowAll;
    private dosen pembina1;
    private dosen pembina2;
    private String pengabdian;
    private String lokasi;

    public HMJ(String pengabdian, String lokasi) {
        this.pengabdian = pengabdian;
        this.lokasi = lokasi;
    }

    public mahasiswa getKetua() {
        return ketua;
    }

    public void setKetua(mahasiswa ketua) {
        this.ketua = ketua;
    }

    public mahasiswa getSekre() {
        return sekre;
    }

    public void setSekre(mahasiswa sekre) {
        this.sekre = sekre;
    }

    public mahasiswa getBendahara() {
        return bendahara;
    }

    public void setBendahara(mahasiswa bendahara) {
        this.bendahara = bendahara;
    }

    public dosen getPembina1() {
        return pembina1;
    }

    public void setPembina1(dosen pembina1) {
        this.pembina1 = pembina1;
    }

    public dosen getPembina2() {
        return pembina2;
    }

    public void setPembina2(dosen pembina2) {
        this.pembina2 = pembina2;
    }

    public String getPengabdian() {
        return pengabdian;
    }

    public void setPengabdian(String pengabdian) {
        this.pengabdian = pengabdian;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }
    
    
    public void ShowAll(){
        System.out.print("ketua      :" + ketua.getNama() + " (" +ketua.getNim()+")("+ ketua.getNilaiAkhir()+") "); ketua.ToString();
        System.out.print("Sekre      :" + sekre.getNama() + " (" +sekre.getNim()+")("+ sekre.getNilaiAkhir()+") "); sekre.ToString();
        System.out.print("bendahara  :" + bendahara.getNama() + " (" +bendahara.getNim()+")(" +bendahara.getNilaiAkhir()+") ");bendahara.ToString();
        System.out.println("pembina1   :" + pembina1.getNama() + " (" +pembina1.getNpp()+")");
        System.out.println("pembina2   :" + pembina2.getNama() + " (" +pembina2.getNpp()+")");
        System.out.println("pengabdian :" + pengabdian + " " + lokasi+ " ");
    }
}
