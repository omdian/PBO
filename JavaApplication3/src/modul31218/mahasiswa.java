/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul31218;

/**
 *
 * @author admin
 */
public class mahasiswa {

    private String nama;
    private String nim;
    private double nilai_tugas;
    private double nilai_uts;
    private double nilai_uas;
    public static double bobot_uas = 0.35;

    public mahasiswa() {
    }

    public mahasiswa(String nama, String nim, double nilai_tugas, double nilai_uts, double nilai_uas) {
        this.nama = nama;
        this.nim = nim;
        this.nilai_tugas = nilai_tugas;
        this.nilai_uts = nilai_uts;
        this.nilai_uas = nilai_uas;
    }

    

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public double getNilai_tugas() {
        return nilai_tugas;
    }

    public void setNilai_tugas(double nilai_tugas) throws Exception {
        if (nilai_tugas >= 0 && nilai_tugas <= 100) {
            this.nilai_tugas = nilai_tugas;
        } else {
            throw new Exception("invalid number");
        }

    }

    public double getNilai_uts() {
        return nilai_uts;
    }

    public void setNilai_uts(double nilai_uts) throws Exception {
        if (nilai_uts >= 0 && nilai_uts <= 100) {
            this.nilai_uts = nilai_uts;
        } else {
            throw new Exception("invalid number");
        }

    }

    public double getNilai_uas() {
        return nilai_uas;
    }

    public void setNilai_uas(double nilai_uas) throws Exception {
        if (nilai_uas >= 0 && nilai_uas <= 100) {
            if (nilai_tugas != 0 && nilai_uts != 0) {
                this.nilai_uas = nilai_uas;
            } else {
                throw new Exception("tugas dan uts tidak boleh kosong");
            }

        } else {
            throw new Exception("invalid number");
        }

    }

    public static double getBobot_uas() {
        return bobot_uas;
    }

    public static void setBobot_uas(double bobot_uas) {
        mahasiswa.bobot_uas = bobot_uas;
    }

    public double getNilaiAkhir() {
        return bobot_uas * nilai_uas + 0.25 * nilai_uts + 0.40 * nilai_tugas;
    }

    public void ToString(){
         if (getNilaiAkhir() >= 80)
             System.out.println("A");
         else if(getNilaiAkhir() <= 80 && getNilaiAkhir() >= 75)
             System.out.println("B");
         else if(getNilaiAkhir() <= 75 && getNilaiAkhir() >= 60)
             System.out.println("C");
         else if(getNilaiAkhir() <= 60 && getNilaiAkhir() >= 45)
             System.out.println("D");
         else 
             System.out.println("F");
    }
}
