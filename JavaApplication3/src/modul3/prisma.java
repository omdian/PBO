/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul3;

import java.util.Scanner;

/**
 *
 * @author admin
 */
public class prisma {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        System.out.println("Penghitungan volume, dan luas alas bangun Prisma segitiga");
        System.out.print("Masukkan tinggi alas segitiga : ");
        double TA = in.nextDouble();
        System.out.print("Masukkan panjang alas segitiga : ");
        double PA = in.nextDouble();
        System.out.print("Masukkan tinggi prisma : ");
        double T = in.nextDouble();

        double L = (PA * TA) / 2;
        double V = (L * T);
        System.out.println("Luas alas segitiga = " + L);
        System.out.println("Volume Prisma = " + V);
}
}


