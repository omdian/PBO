/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul3;

import javax.swing.JOptionPane;

/**
 *
 * @author admin
 */
public class main {
    public static void main(String[] args) {
        segitiga seg1=new segitiga(); // membentuk obyek seg1 dari class Segitiga
        seg1.x1=1; seg1.y1=2;	// isi data titik pertama (1,2) ke obyek seg1
        seg1.x2=7; seg1.y2=2;   // isi data titik kedua (7,2) ke obyek seg2
        seg1.x3=7; seg1.y3=9;	// isi data titik ketiga (7,9) ke obyek seg3
        JOptionPane.showMessageDialog(null, "Luas segitiga dengan titik (1,2) (7,2) dan (7,9) adalah :"+ seg1.hitungLuas());
        
        segitiga seg2=new segitiga(); // membentuk obyek seg2 dari class Segitiga
        seg2.x1=0; seg2.y1=0;	// pengisian data 3 titik segitiga
        seg2.x2=5; seg2.y2=4;
        seg2.x3=8; seg2.y3=2;
        JOptionPane.showMessageDialog(null, "Luas segitiga dengan titik (0,0) (5,4) dan (8,2) adalah :"+ seg2.hitungLuas());
    }
}
