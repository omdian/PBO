/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3B;

/**
 *
 * @author admin
 */
public class titik {

    private double x;
    private double y;

    public titik() {
    }

    public titik(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getKoordinat() {//fungsi untuk menampilkan koordinat
        return "(" + x + "," + y + ")";
    }

    public static double hitJarak(titik t1, titik t2) { //fungsi untuk mencari jarak 2 titik
        double dx = t1.x - t2.x;
        double dy = t1.y - t2.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public titik(titik t1, titik t2) {//construktor untuk mencari titik tengah
        x = (t1.x + t2.x) / 2;
        y = (t1.y + t2.y) / 2;
    }
}
