/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3B;

/**
 *
 * @author admin
 */
public class maintitik {

    public static void main(String[] args) {
        titik t1 = new titik();//untuk membuat objek t1
        t1.setX(1); //memasukkan nilai untuk variabel x
        t1.setY(6); // memasukkan nilai untuk variabel y
        System.out.println("Menampilkan info Kelas Titik t1 : ");
        System.out.println(t1.getKoordinat());

        titik t2 = new titik(10, 6);
        System.out.println("Menampilkan info Kelas Titik t2 : ");
        System.out.println(t2.getKoordinat());

        System.out.println("jarak dua titik P: " + titik.hitJarak(t1, t2));

        titik t3 = new titik(t1, t2);
        System.out.println("Titik Tengah :(" + t3.getX() + "," + t3.getY() + ")");

    }
}
